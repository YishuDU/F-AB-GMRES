function [x,k] = nesor1(AT, x0, b, tol, maxit, omega)
global diag_AAt B
[~,m] = size(AT);
x=x0;
%r=b-A*x;
r=b;
normr0=norm(r);
for k = 1:maxit
  for  i = 1:m
%       ai = AT(:,i)';
%       temp = omega*(b(i)-ai*x) / diag_AAt(i);
%       x = x + temp*ai';
      temp = omega*r(i)/diag_AAt(i);
      x = x + temp*AT(:,i);
      %r=b-A*x;
      b_ik=B(:,i);
      r = r - temp*b_ik;
  end
  rre=norm(r)/normr0;
  if rre < tol
      break
  end 
end