clear
load('C:\Users\DELL\Desktop\test\data\RANDL1.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL1T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL1T


clear
load('C:\Users\DELL\Desktop\test\data\RANDL2.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL2T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL2T


clear
load('C:\Users\DELL\Desktop\test\data\RANDL3.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL3T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL3T

clear
load('C:\Users\DELL\Desktop\test\data\RANDL4.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL4T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL4T

clear
load('C:\Users\DELL\Desktop\test\data\RANDL5.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL5T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL5T



clear
load('C:\Users\DELL\Desktop\test\data\RANDL6.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL6T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL6T


clear
load('C:\Users\DELL\Desktop\test\data\illc1850_nonzero.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save illc1850T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save illc1850T


clear
load('C:\Users\DELL\Desktop\test\data\gen_nonzero.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save genT_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save genT



clear
load('C:\Users\DELL\Desktop\test\data\photogrammetry2_nonzero.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save photogrammetry2T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save photogrammetry2T


clear
load('C:\Users\DELL\Desktop\test\data\Maragal3_nonzero.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save Maragal3T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save Maragal3T


clear
load('C:\Users\DELL\Desktop\test\data\Maragal4_nonzero.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save Maragal4T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save Maragal4T


clear
load('C:\Users\DELL\Desktop\test\data\Maragal5_nonzero.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save Maragal5T_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save Maragal5T
















clear
load('C:\Users\DELL\Desktop\test\data\RANDL1.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL1_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL1


clear
load('C:\Users\DELL\Desktop\test\data\RANDL2.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL2_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL2


clear
load('C:\Users\DELL\Desktop\test\data\RANDL3.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL3_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL3

clear
load('C:\Users\DELL\Desktop\test\data\RANDL4.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL4_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL4

clear
load('C:\Users\DELL\Desktop\test\data\RANDL5.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL5_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL5



clear
load('C:\Users\DELL\Desktop\test\data\RANDL6.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save RANDL6_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save RANDL6


clear
load('C:\Users\DELL\Desktop\test\data\illc1850_nonzero.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save illc1850_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save illc1850


clear
load('C:\Users\DELL\Desktop\test\data\gen_nonzero.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save gen_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save gen



clear
load('C:\Users\DELL\Desktop\test\data\photogrammetry2_nonzero.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save photogrammetry2_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save photogrammetry2


clear
load('C:\Users\DELL\Desktop\test\data\Maragal3_nonzero.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save Maragal3_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save Maragal3


clear
load('C:\Users\DELL\Desktop\test\data\Maragal4_nonzero.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save Maragal4_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save Maragal4


clear
load('C:\Users\DELL\Desktop\test\data\Maragal5_nonzero.mat')
% A=A';
% b=A*x_initial_starT;
b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc

%% 
for ii=1:10
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0] = innerABGMRES( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3] = innerABFGMRES_B( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4] = innerABFGMRES_B( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner);%GK
    it0_total(ii)=it0;
    it1_total(ii)=it1;
    it2_total(ii)=it2;
    it3_total(ii)=it3;
    it4_total(ii)=it4;
    time0_total(ii)=time0;
    time1_total(ii)=time1;
    time2_total(ii)=time2;
    time3_total(ii)=time3;
    time4_total(ii)=time4;
    rse0_total(ii)=norm(x0-x_exact)/norm(x_exact);
    rse1_total(ii)=norm(x1-x_exact)/norm(x_exact);
    rse2_total(ii)=norm(x2-x_exact)/norm(x_exact);
    rse3_total(ii)=norm(x3-x_exact)/norm(x_exact);
    rse4_total(ii)=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1(ii) = sum(it_inner1);
    it_inner_total2(ii) = sum(it_inner2);
    it_inner_total3(ii) = sum(it_inner3);
    it_inner_total4(ii) = sum(it_inner4);
    eval(['save Maragal5_',num2str(ii),'.mat'])
end
it0_median=median(it0_total)
it1_median=median(it1_total)
it2_median=median(it2_total)
it3_median=median(it3_total)
it4_median=median(it4_total)
it_inner_final0=l_opt0_NESOR*m*it0_median
it_inner_final1=median(it_inner_total1)
it_inner_final2=median(it_inner_total2)
it_inner_final3=median(it_inner_total3)
it_inner_final4=median(it_inner_total4)
time_median0=median(time0_total);
time_median1=median(time1_total);
time_median2=median(time2_total);
time_median3=median(time3_total);
time_median4=median(time4_total);
time_final0=time_median0+time_B+time0_NESOR
time_final1=time_median1+time_B+time1_K
time_final2=time_median2+time_B+time2_RK
time_final3=time_median3+time_B+time3_GRK
time_final4=time_median4+time_B+time4_GK
rse0=median(rse0_total)
rse1=median(rse1_total)
rse2=median(rse2_total)
rse3=median(rse3_total)
rse4=median(rse4_total)
save Maragal5