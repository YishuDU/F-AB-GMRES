function [x,it] = Randomized_Kaczmarz1(AT, x0, b, tol, maxit, omega)
% 随机Kaczmarz算法
% 输入 矩阵A 初始向量x0 右端项b 容忍误差tol 最大迭代步数maxit 松弛因子omega 最小范数解x_star
% 输出 数值解x 迭代步数iter 计算时间CPU 解误差向量norm_e 收敛标志flag
global diag_AAt prob_diag_AAt B
[~,m] = size(AT);
x = x0;
% r=b-A*x;
r=b;
norm_b=norm(b);
for it=1:maxit
    i_k = randsample(1:m,1,true,prob_diag_AAt);
    temp = omega*r(i_k)/diag_AAt(i_k);
    x = x + temp*AT(:,i_k);
    %r = b - A*x;
    b_ik=B(:,i_k);
    r = r - temp*b_ik;
    rre = norm(r)/norm_b;
%     i_k = randsample(1:m,1,true,prob_diag_AAt);
%     a_ik = AT(:,i_k)';
%     temp = omega*(b(i_k) - a_ik*x)/diag_AAt(i_k);
%     x = x + temp*a_ik';
%     %r = b - A*x;
%     b_ik=B(:,i_k);
%     r = r - temp*b_ik;
%     rre = norm(r)/norm_b;
    if rre < tol
        break
    end 
end
