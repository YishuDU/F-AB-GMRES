function [x,it] = Greedy_Randomized_Kaczmarz1(AT, x0, b, tol, maxit, omega)
% 贪婪随机Kaczmarz算法
% 输入 矩阵A 初始向量x0 右端项b 容忍误差tol 最大迭代步数maxit 松弛因子omega 最小范数解x_star
% 输出 数值解x 迭代步数iter 计算时间CPU 解误差向量norm_e 收敛标志flag
global A_fro diag_AAt B
[~,m] = size(AT);
x = x0;

%r = b-A*x;
r=b;
norm_b=norm(b);
for it = 1:maxit
    norm_r = norm(r)^2;
    ratio = abs(r).^2./diag_AAt;
    max_ratio = max(ratio);
    aipu=1/2*(max_ratio+norm_r/A_fro);  
    r_tilde=r;
    r_tilde(aipu > ratio)=0;
    prob_r = (abs(r_tilde).^2)/(norm(r_tilde)^2);
    %i_k =  randsrc(1,1,[1:m; prob_r']);    
    i_k = randsample(1:m,1,true,prob_r);
    temp = omega*r(i_k)/diag_AAt(i_k);   
    x = x + temp*AT(:,i_k);
    %r = b - A*x;
    b_ik=B(:,i_k);
    r = r - temp*b_ik;   
    rse=norm(r)/norm_b;
    if rse < tol
        break
    end  
end