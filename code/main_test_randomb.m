clear
load('C:\Users\DELL\Desktop\test\data\Maragal5_nonzero.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc


%b=randn(m,1);
epsilon=10^(-3);
rng(1)
rand_number=-1+2*rand;
b=b*(1+epsilon*rand_number);

%% 
%for ii=1:10
    ii=1;
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0,error_new0] = innerABGMRES_randomb( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR,x_exact); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1,error_new1] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner,x_exact); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2,error_new2] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner,x_exact); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3,error_new3] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner,x_exact); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4,error_new4] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner,x_exact);%GK
    rse0_total=norm(x0-x_exact)/norm(x_exact);
    rse1_total=norm(x1-x_exact)/norm(x_exact);
    rse2_total=norm(x2-x_exact)/norm(x_exact);
    rse3_total=norm(x3-x_exact)/norm(x_exact);
    rse4_total=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1 = sum(it_inner1);
    it_inner_total2 = sum(it_inner2);
    it_inner_total3 = sum(it_inner3);
    it_inner_total4 = sum(it_inner4);
%end
time_final0=time0+time0_NESOR
time_final1=time1+time_B+time1_K
time_final2=time2+time_B+time2_RK
time_final3=time3+time_B+time3_GRK
time_final4=time4+time_B+time4_GK
save Maragal5T_1_random_b_epsilon_10_-3





clear
load('C:\Users\DELL\Desktop\test\data\Maragal5_nonzero.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc


%b=randn(m,1);
epsilon=10^(-2);
rng(1)
rand_number=-1+2*rand;
b=b*(1+epsilon*rand_number);

%% 
%for ii=1:10
    ii=1;
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0,error_new0] = innerABGMRES_randomb( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR,x_exact); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1,error_new1] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner,x_exact); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2,error_new2] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner,x_exact); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3,error_new3] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner,x_exact); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4,error_new4] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner,x_exact);%GK
    rse0_total=norm(x0-x_exact)/norm(x_exact);
    rse1_total=norm(x1-x_exact)/norm(x_exact);
    rse2_total=norm(x2-x_exact)/norm(x_exact);
    rse3_total=norm(x3-x_exact)/norm(x_exact);
    rse4_total=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1 = sum(it_inner1);
    it_inner_total2 = sum(it_inner2);
    it_inner_total3 = sum(it_inner3);
    it_inner_total4 = sum(it_inner4);
%end
time_final0=time0+time0_NESOR
time_final1=time1+time_B+time1_K
time_final2=time2+time_B+time2_RK
time_final3=time3+time_B+time3_GRK
time_final4=time4+time_B+time4_GK
save Maragal5T_1_random_b_epsilon_10_-2






clear
load('C:\Users\DELL\Desktop\test\data\Maragal5_nonzero.mat')
A=A';
b=A*x_initial_starT;
% b=A*x_initial_star;
[m,n]=size(A);
x00=zeros(n,1);
maxit_inner=200000;
tol_inner=10^(-1); 
AT=A';
maxit_outer=2000;
tol_outer=1e-6;
x_exact=pinv(full(A))*b;

global diag_AAt diag_AAt_sqrt prob_diag_AAt B A_fro 
diag_AAt = sum(abs(A).^2,2); %the square of the norm of the row
diag_AAt_sqrt = sqrt(diag_AAt);
A_fro =  sum(diag_AAt);
prob_diag_AAt = full(diag_AAt / A_fro);
tic
B=A*A';
time_B=toc;

%% NE-SOR
omega=1;
tic
[~,l_opt0_NESOR] = nesor1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = nesor2(AT, x00, b, l_opt0_NESOR, omega);
end
[~,b1]=min(res_omega);
omega_opt0_NESOR=b1*0.1
time0_NESOR=toc

%% K
omega=1;
tic
[~,l_opt1_K] = Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Kaczmarz2(AT, x00, b, l_opt1_K, omega);
end
[~,b1]=min(res_omega);
omega_opt1_K=b1*0.1
time1_K=toc

%% RK
omega=1;
l_opt2_RK_total=zeros(1,10);
tic
for i=1:10
    rng(i);
    [~,l_opt2_RK_total(i)] = Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt2_RK=median(l_opt2_RK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Randomized_Kaczmarz2(AT, x00, b, l_opt2_RK, omega);
end
[~,b1]=min(res_omega);
omega_opt2_RK=b1*0.1
time2_RK=toc

%% GRK
omega=1;
tic
l_opt3_GRK_total=zeros(1,10);
for i=1:10
    rng(i);
    [~,l_opt3_GRK_total(i)] = Greedy_Randomized_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);
end
l_opt3_GRK=median(l_opt3_GRK_total);

i=0;
for omega=0.1:0.1:1.9
    rng(1);
    i=i+1;
    [~,res_omega(i)] = Greedy_Randomized_Kaczmarz2(AT, x00, b, l_opt3_GRK, omega);
end
[~,b1]=min(res_omega);
omega_opt3_GRK=b1*0.1
time3_GRK=toc

%% GK
omega=1;
tic
[~,l_opt4_GK] = Greedy_Kaczmarz1(AT, x00, b, tol_inner, maxit_inner, omega);

i=0;
for omega=0.1:0.1:1.9
    i=i+1;
    [~,res_omega(i)] = Greedy_Kaczmarz2(AT, x00, b, l_opt4_GK, omega);
end
[~,b1]=min(res_omega);
omega_opt4_GK=b1*0.1
time4_GK=toc


%b=randn(m,1);
epsilon=10^(-1);
rng(1)
rand_number=-1+2*rand;
b=b*(1+epsilon*rand_number);

%% 
%for ii=1:10
    ii=1;
    [x0, it0, time0, flag0, res0, relvec0, ts0,relvec_new0,error_new0] = innerABGMRES_randomb( A, x00, b, maxit_outer, tol_outer, l_opt0_NESOR, omega_opt0_NESOR,x_exact); %NE-SOR
    [x1, it1, time1, flag1, res1, relvec1, ts1, it_inner1,relvec_new1,error_new1] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer,tol_outer, 1, l_opt1_K, omega_opt1_K,tol_inner,x_exact); %K 
    rng(ii);
    [x2, it2, time2, flag2, res2, relvec2, ts2, it_inner2,relvec_new2,error_new2] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer,tol_outer, 2, l_opt2_RK, omega_opt2_RK,tol_inner,x_exact); %RK 
    rng(ii);
    [x3, it3, time3, flag3, res3, relvec3, ts3, it_inner3,relvec_new3,error_new3] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer, tol_outer, 3, l_opt3_GRK, omega_opt3_GRK,tol_inner,x_exact); %GRK
    [x4, it4, time4, flag4, res4, relvec4, ts4, it_inner4,relvec_new4,error_new4] = innerABFGMRES_B_randomb( A, x00, b, maxit_outer,tol_outer, 4, l_opt4_GK, omega_opt4_GK,tol_inner,x_exact);%GK
    rse0_total=norm(x0-x_exact)/norm(x_exact);
    rse1_total=norm(x1-x_exact)/norm(x_exact);
    rse2_total=norm(x2-x_exact)/norm(x_exact);
    rse3_total=norm(x3-x_exact)/norm(x_exact);
    rse4_total=norm(x4-x_exact)/norm(x_exact);
    it_inner_total1 = sum(it_inner1);
    it_inner_total2 = sum(it_inner2);
    it_inner_total3 = sum(it_inner3);
    it_inner_total4 = sum(it_inner4);
%end
time_final0=time0+time0_NESOR
time_final1=time1+time_B+time1_K
time_final2=time2+time_B+time2_RK
time_final3=time3+time_B+time3_GRK
time_final4=time4+time_B+time4_GK
save Maragal5T_1_random_b_epsilon_10_-1