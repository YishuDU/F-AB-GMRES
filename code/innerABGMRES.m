function [x, iter, time, flag, res, relvec, time_sep, res_new] = innerABGMRES( A, x0, b, maxit, tol, maxit_inner, omega_inner)
%采用AB-GMRES ABu=b x=Bu 
%采用右预处理GMRES 求解线性方程组Ax=b
%输入 系数矩阵A 初始向量x 右端项b 无重启动 最大迭代步数maxit 容忍误差tol
%输出 解x 迭代步数iter CPU时间time 收敛标志flag: flag=0表示在maxit次迭代内收敛至所需容差tol 残差res                                 
flag = 1;
[m,n] = size(A);
relvec=zeros(maxit,1);
relvec(1)=1;
res_new=zeros(maxit,1);
res_new(1)=1;
V(1:m,1:maxit+1) = zeros(m,maxit+1);
H(1:maxit+1,1:maxit) = zeros(maxit+1,maxit);
cs(1:maxit) = zeros(maxit,1);
sn(1:maxit) = zeros(maxit,1);
e1    = zeros(n,1);
e1(1) = 1.0;
AT=A';
time_sep=zeros(1,maxit);

r =  b-A*x0 ;
rnrm2 = norm(r);
V(:,1) = r / rnrm2;
s = rnrm2*e1;

for iter = 1:maxit 
    tic
    %z = B*V(:,iter);
    z = nesor(AT,zeros(n,1),V(:,iter), maxit_inner, omega_inner); %NE-SOR 求解Az=v 等价于AA'y=v  
    w = A*z;
    for k = 1:iter
        H(k,iter)= w'*V(:,k);
        w = w - H(k,iter)*V(:,k);
    end
    H(iter+1,iter) = norm( w );
    V(:,iter+1) = w / H(iter+1,iter);
    for k = 1:iter-1
        temp     =  cs(k)*H(k,iter) + sn(k)*H(k+1,iter);
        H(k+1,iter) = -sn(k)*H(k,iter) + cs(k)*H(k+1,iter);
        H(k,iter)   = temp;
    end
    [cs(iter),sn(iter)] = rotmat( H(iter,iter), H(iter+1,iter) );
    temp   = cs(iter)*s(iter);
    s(iter+1) = -sn(iter)*s(iter);
    s(iter)   = temp;   
    H(iter,iter) = cs(iter)*H(iter,iter) + sn(iter)*H(iter+1,iter);
    H(iter+1,iter) = 0.0;
    res  = abs(s(iter+1)) / rnrm2;
    relvec(iter+1)=res;
    time_sep(iter)=toc;
    
    y = H(1:iter,1:iter) \ s(1:iter);
    u = V(:,1:iter)*y;
    z = nesor(AT,zeros(n,1),u, maxit_inner, omega_inner);
    x = x0 + z;
    res_new(iter+1) = norm(b-A*x)/norm(b);
    
    tic
    if ( res <= tol )
        y = H(1:iter,1:iter) \ s(1:iter);
        u = V(:,1:iter)*y;
        z = nesor(AT,zeros(n,1),u, maxit_inner, omega_inner);
        x = x0 + z;
        break
    end
    t_end=toc;
end
%time = toc;
time = sum(time_sep)+t_end;
if ( norm(b-A*x)/norm(b) < tol )
    flag = 0;
end                 