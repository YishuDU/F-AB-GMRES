function [x,rre] = Greedy_Kaczmarz2(AT, x0, b, maxit, omega)
% 贪婪Kaczmarz算法
% 输入 矩阵A 初始向量x0 右端项b 容忍误差tol 最大迭代步数maxit 松弛因子omega 最小范数解x_star
% 输出 数值解x 迭代步数iter 计算时间CPU 解误差向量norm_e 收敛标志flag
global diag_AAt B
x = x0;
% r = b-A*x;
r=b;
[~,i_k] = max(abs(r));
norm_b=norm(b);
for it=1:maxit
    temp = omega*r(i_k)/diag_AAt(i_k);
    x = x + temp*AT(:,i_k);
    %r = b - A*x;
    b_ik=B(:,i_k);
    r = r - temp*b_ik;
    [~,i_k]=max(abs(r));
    rre = norm(r)/norm_b;
end