function [ c, s ] = rotmat( a, b )
  %计算给定的a和b的Givens旋转矩阵参数
   if ( b == 0.0 )
      c = 1.0;
      s = 0.0;
   elseif ( abs(b) > abs(a) )
      temp = a / b;
      s = 1.0 / sqrt( 1.0 + temp^2 );
      c = temp * s;
   else
      temp = b / a;
      c = 1.0 / sqrt( 1.0 + temp^2 );
      s = temp * c;
   end