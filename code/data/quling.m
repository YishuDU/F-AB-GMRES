clear
clc
load('gen.mat')

A=getfield(Problem,'A');
[m1,n1]=size(A);
A(all(A == 0, 2),:) = [];
A(:,all(A == 0, 1)) = [];
[m,n]=size(A);
x_initial_star=randn(n,1);
x_initial_starT=randn(m,1);
save gen_nonzero A x_initial_star x_initial_starT

