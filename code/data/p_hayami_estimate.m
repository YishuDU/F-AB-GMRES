clear
load('C:\Users\DELL\Desktop\F-AB-GMRES-code\data\photogrammetry2_nonzero.mat')
%A=A';
A=full(A);
[m,n]=size(A)
[nz,~]=size(nonzeros(A))
q=nz/m;
d=sum(sum(A~=0))/(m*n)
p1=1-(1-1/m)*(1-d^2)^n-1/m*(1-d)^n
p2=1-(1-1/m)*exp(-n*d^2)-1/m*exp(-q)
AAT=A*A';
p_actual=sum(sum(AAT~=0))/m^2

