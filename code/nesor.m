function x = nesor(AT, x0, b, maxit, omega)
global diag_AAt B
[~,m] = size(AT);
x=x0;
r=b;
for k = 1:maxit
  for  i = 1:m
%       ai = AT(:,i)';
%       temp = omega*(b(i)-ai*x) / diag_AAt(i);
%       x = x + temp*ai';
      temp = omega*r(i)/diag_AAt(i);
      x = x + temp*AT(:,i);
      %r=b-A*x;
      b_ik=B(:,i);
      r = r - temp*b_ik;
  end
end