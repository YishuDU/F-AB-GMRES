clear
close all
%load('C:\Users\DELL\Desktop\test\data_final20210206\RANDL6T_7.mat') 
%load('C:\Users\DELL\Desktop\test\data_final20210206\photogrammetry2T_10.mat')
%load('C:\Users\DELL\Desktop\test\data_final20210206\Maragal5T_4.mat')
%load('C:\Users\DELL\Desktop\test\data_final20210206\photogrammetry2_10.mat')
%load('C:\Users\DELL\Desktop\test\data_final20210206\Maragal5_1.mat')
load('C:\Users\DELL\Desktop\test\Maragal5T_1_random_b.mat')


for i=1:it1
    it_inner1_plot(i)=sum(it_inner1(1:i));
end

for i=1:it2
    it_inner2_plot(i)=sum(it_inner2(1:i));
end

for i=1:it3
    it_inner3_plot(i)=sum(it_inner3(1:i));
end

for i=1:it4
    it_inner4_plot(i)=sum(it_inner4(1:i));
end

for i=1:it0
    ts0_plot(i)=sum(ts0(1:i));
end

for i=1:it1
    ts1_plot(i)=sum(ts1(1:i));
end

for i=1:it2
    ts2_plot(i)=sum(ts2(1:i));
end

for i=1:it3
    ts3_plot(i)=sum(ts3(1:i));
end

for i=1:it4
    ts4_plot(i)=sum(ts4(1:i));
end

% % plot inner iterations
% semilogy(l_opt0_NESOR*m*[0:it0],relvec_new0(1:it0+1),'g-o'); 
% hold on
% semilogy([0,it_inner1_plot],relvec_new1(1:it1+1),'m--'); 
% hold on 
% semilogy([0,it_inner2_plot],relvec_new2(1:it2+1),'r:','linewidth',1); 
% hold on 
% semilogy([0,it_inner3_plot],relvec_new3(1:it3+1),'k-.'); 
% hold on 
% semilogy([0,it_inner4_plot],relvec_new4(1:it4+1),'b-'); 

% semilogy(l_opt0_NESOR*m*[0:it0],relvec0(1:it0+1),'b-o'); 
% hold on
% semilogy([0,it_inner1_plot],relvec1(1:it1+1),'g--'); 
% hold on 
% semilogy([0,it_inner2_plot],relvec2(1:it2+1),'m:','linewidth',1); 
% hold on 
% semilogy([0,it_inner3_plot],relvec3(1:it3+1),'r-.'); 
% hold on 
% semilogy([0,it_inner4_plot],relvec4(1:it4+1),'k-'); 
% 
% %title('RANDL6T');
% %title('photogrammetry2');
% title('Maragal\_5');
% xlabel('Total number of inner iterations') 
% ylabel('Relative residual norm')
% legend('NE-SOR','K','RK','GRK','GK')






% %plot CPU
% semilogy(time0_NESOR+[0,ts0_plot(1:it0)],relvec_new0(1:it0+1),'g-o'); 
% hold on
% semilogy(time_B+time1_K+[0,ts1_plot(1:it1)],relvec_new1(1:it1+1),'m--'); 
% hold on 
% semilogy(time_B+time2_RK+[0,ts2_plot(1:it2)],relvec_new2(1:it2+1),'r:','linewidth',1); 
% hold on 
% semilogy(time_B+time3_GRK+[0,ts3_plot(1:it3)],relvec_new3(1:it3+1),'k-.'); 
% hold on 
% semilogy(time_B+time4_GK+[0,ts4_plot(1:it4)],relvec_new4(1:it4+1),'b-'); 

% semilogy(time0_NESOR+[0,ts0_plot(1:it0)],relvec0(1:it0+1),'b-o'); 
% hold on
% semilogy(time_B+time1_K+[0,ts1_plot(1:it1)],relvec1(1:it1+1),'g--'); 
% hold on 
% semilogy(time_B+time2_RK+[0,ts2_plot(1:it2)],relvec2(1:it2+1),'m:','linewidth',1); 
% hold on 
% semilogy(time_B+time3_GRK+[0,ts3_plot(1:it3)],relvec3(1:it3+1),'r-.'); 
% hold on 
% semilogy(time_B+time4_GK+[0,ts4_plot(1:it4)],relvec4(1:it4+1),'k-'); 
% 
% %title('RANDL6T');
% %title('photogrammetry2');
% title('Maragal\_5');
% xlabel('CPU time [seconds]') 
% ylabel('Relative residual norm')
% legend('NE-SOR','K','RK','GRK','GK')


%% random b
% % plot relative residual norm
% semilogy(l_opt0_NESOR*m*[0:it0],relvec_new0(1:it0+1),'b-o'); 
% hold on
% semilogy([0,it_inner1_plot],relvec_new1(1:it1+1),'g--'); 
% hold on 
% semilogy([0,it_inner2_plot],relvec_new2(1:it2+1),'m:','linewidth',1); 
% hold on 
% semilogy([0,it_inner3_plot],relvec_new3(1:it3+1),'r-.'); 
% hold on 
% semilogy([0,it_inner4_plot],relvec_new4(1:it4+1),'k-'); 
% 
% title('Maragal\_5T');
% xlabel('Total number of inner iterations') 
% ylabel('Relative residual norm')
% legend('NE-SOR','K','RK','GRK','GK')


% plot relative error norm
semilogy(l_opt0_NESOR*m*[0:it0],error_new0(1:it0+1),'b-o'); 
hold on
semilogy([0,it_inner1_plot],error_new1(1:it1+1),'g--'); 
hold on 
semilogy([0,it_inner2_plot],error_new2(1:it2+1),'m:','linewidth',1); 
hold on 
semilogy([0,it_inner3_plot],error_new3(1:it3+1),'r-.'); 
hold on 
semilogy([0,it_inner4_plot],error_new4(1:it4+1),'k-'); 

title('Maragal\_5T');
xlabel('Total number of inner iterations') 
ylabel('Relative error norm')
legend('NE-SOR','K','RK','GRK','GK')

